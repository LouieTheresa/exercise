<!DOCTYPE html>
<html>
<head>
<title>
	Solve problems using SQL
</title>
	<style type="text/css">
		table{
			border:1;
			padding: 3px;
		}
		tr,td,th{
			padding: 4px;
		}
		body{
			margin: 1%;
		}
		button{
			margin-bottom: 7px;
		}
	</style>

</head>

<body>
	<form method="POST">
	
		<button name="btnlnk">1) Retrieve employees whose last name start with "K".</button>
		<br>
		<button name="btnlni">2) Retrieve employees whose last name end with "i".</button>		
		<br>
		<button name="btnhd">3) Retrieve employee's full name and their hire date whose hire date is between 2015/1/1 and 2015/3/31 ordered by ascending by hire date.</button>		
		<br>
		<button name="btnln">4) Retrieve employee's last name and their boss's last name. If they don't have boss, no need to retrieve and show.</button>
		<br>
		<button name="btnsd">5) Retrieve employee's last name who belong to Sales department ordered by descending by last name.</button>
		<br>
		<button name="btnmdn">6) Retrieve number of employee who has middle name.</button>
		<br>
		<button name="btndnc">7) Retrieve department name and number of employee in each department. You don't need to retrieve the department name which doesn't have employee.</button>
		<br>
		<button name="btnfnhd">8) Retrieve employee's full name and hire date who was hired the most recently.</button>
		<br>
		<button name="btnoemdpt">9) Retrieve department name which has no employee.</button>
		<br>
		<button name="btnmp">10) Retrieve employee's full name who has more than 2 positions.</button>
		<br><br>
		</form>
		<center>

<?php
		function opencon(){
				$dbhost = "localhost";
				$dbuser = "root";
				$password = "";
				$db = "sqlproblems";

				$conn = new mysqli($dbhost, $dbuser, $password, $db) or die("Connection Failed." . $conn -> error);
				return  $conn;
		}

				function closecon($conn){
				$conn -> close();
				}

			$con = opencon();

			if(!isset($_POST['btnlnk'])){
			}else{
				$result = "SELECT * FROM employees Where last_name LIKE 'K%'";
				$sql = $con -> query($result);
				echo"<center><table border = '1'>
				<tr><th>ID</th><th>First Name</th><th>Last Name</th><th>Middle Name</th><th>Hire Date</th><th>Boss ID</th></tr>";

				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['id'] . "</td>";
				echo "<td>" . $row['first_name'] . "</td>";
				echo "<td>" . $row['last_name'] . "</td>";
				echo "<td>" . $row['middle_name'] . "</td>";
				echo "<td>" . $row['hire_date'] . "</td>";
				echo "<td>" . $row['boss_id'] . "</td>";
				echo "</tr>";	
				}
				echo "</table>";

			}
			
			if(!isset($_POST['btnlni'])){
			}else{
				$result = "SELECT * FROM employees Where last_name LIKE '%i'";
				$sql = $con -> query($result);
				echo"<center><table border = '1'>
				<tr><th>ID</th><th>First Name</th><th>Last Name</th><th>Middle Name</th><th>Hire Date</th><th>Boss ID</th></tr>";

				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['id'] . "</td>";
				echo "<td>" . $row['first_name'] . "</td>";
				echo "<td>" . $row['last_name'] . "</td>";
				echo "<td>" . $row['middle_name'] . "</td>";
				echo "<td>" . $row['hire_date'] . "</td>";
				echo "<td>" . $row['boss_id'] . "</td>";
				echo "</tr>";	
				}
				echo "</table>";

			}
			if(!isset($_POST['btnhd'])){
			}else{
				$result = "SELECT CONCAT(last_name,', ',first_name,' ',middle_name) AS full_name,hire_date FROM employees Where hire_date BETWEEN '2015/1/1' AND '2015/3/31' ORDER BY hire_date ASC";
				$sql = $con -> query($result);
				echo"<center><table border = '1'>
				<tr><th>Full Name</th><th>Hire Date</th></tr>";

				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['full_name'] . "</td>";
				echo "<td>" . $row['hire_date'] . "</td>";
				echo "</tr>";	
				}
				echo "</table>";

			}
			if(!isset($_POST['btnln'])){
			}else{
				$result = "select a.last_name as boss_name,b.last_name as emp_name from employees a inner join employees b on a.id = b.boss_id";
				$sql = $con -> query($result);
				echo"<center><table border = '1'>
				<tr><th>Boss Last Name</th><th>Employee Last Name</th></tr>";

				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['boss_name'] . "</td>";
				echo "<td>" . $row['emp_name'] . "</td>";
				echo "</tr>";	
				}
				echo "</table>";

			}

			if(!isset($_POST['btnsd'])){
			}else{
				$result = "SELECT last_name FROM employees JOIN departments on employees.department_id = departments.id Where employees.department_id = 3 ORDER BY last_name DESC";
				$sql = $con -> query($result);
				echo"<center><table border = '1'>
				<tr><th>Last Name</th></tr>";

				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['last_name'] . "</td>";
				echo "</tr>";	
				}
				echo "</table>";

			}

			if(!isset($_POST['btnmdn'])){
			}else{
				$result = "SELECT Count(middle_name) FROM employees";
				$sql = $con -> query($result);
				echo"<center><table border = '1'>
				<tr><th>Count</th></tr>";

				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['Count(middle_name)'] . "</td>";
				echo "</tr>";	
				}
				echo "</table>";

			}	

			if(!isset($_POST['btndnc'])){
			}else{
				$result = "SELECT Count(employees.id),name FROM employees JOIN departments on employees.department_id = departments.id GROUP BY departments.name";
				$sql = $con -> query($result);
				echo"<center><table border = '1'>
				<tr><th>Count</th><th>Department Name</th></tr>";

				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['Count(employees.id)'] . "</td>";
				echo "<td>" . $row['name'] . "</td>";
				echo "</tr>";	
				}
				echo "</table>";

			}		

			if(!isset($_POST['btnfnhd'])){
			}else{
				$result = "SELECT CONCAT(last_name,first_name,middle_name) as full_name,hire_date FROM employees ORDER BY hire_date DESC LIMIT 1";
				$sql = $con -> query($result);
				echo"<center><table border = '1'>
				<tr><th>Full Name</th><th>Hire Date</th></tr>";

				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['full_name'] . "</td>";
				echo "<td>" . $row['hire_date'] . "</td>";
				echo "</tr>";	
				}
				echo "</table>";

			}	

			if(!isset($_POST['btnoemdpt'])){
			}else{
				$result = "SELECT name FROM departments LEFT JOIN employees on departments.id = employees.department_id WHERE employees.department_id is null";
				$sql = $con -> query($result);
				echo"<center><table border = '1'>
				<tr><th>Department Name</th></tr>";

				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['name'] . "</td>";
				echo "</tr>";	
				}
				echo "</table>";

			}	

			if(!isset($_POST['btnmp'])){
			}else{
				$result = "SELECT Count(employees.id),CONCAT(last_name,', ',first_name) AS full_name FROM employees JOIN employee_positions on employees.id = employee_positions.employee_id GROUP BY employee_positions.employee_id HAVING Count(employees.id) > 1";
				$sql = $con -> query($result);
				echo"<center><table border = '1'>
				<tr><th>Full Name</th></tr>";

				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['full_name'] . "</td>";
				echo "</tr>";	
				}
				echo "</table>";

			}	
			closecon($con);
				?>

				
</body>
</html>