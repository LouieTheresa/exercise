<html>
	<head>
		<title>
			Input date. Then show 3 days from inputted date and its day of the week.
		</title>
	</head>

<body>

<form method="POST">
	<center>
	<br>
	<br>
		Enter Date:
	<input type="date" name="inputdate"/>
	<br>
	<br>
	<input type="submit" name="submitdate">
	<br>
	<br>

</form>

<?php
	if(isset($_POST['submitdate'])){
		if (empty($_POST['inputdate'])){
			echo "Please enter a valid date.";
		}else{
			$datein = $_POST['inputdate'];
		$days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		$noday1 = date('w', strtotime('+1 day', strtotime($datein)));
		$noday2 = date('w', strtotime('+2 day', strtotime($datein)));
		$noday3 = date('w', strtotime('+3 day', strtotime($datein)));
		echo date('d-m-Y', strtotime('+1 day', strtotime($datein))). "($days[$noday1])";
		echo '<br>';
		echo '<br>';
		echo date('d-m-Y', strtotime('+2 day', strtotime($datein))). "($days[$noday2])";
		echo '<br>';
		echo '<br>';
		echo date('d-m-Y', strtotime('+3 day', strtotime($datein))). "($days[$noday3])";
		echo '<br>';
		echo '<br>';
		}
		
	}
		
?>

</body>

</html>