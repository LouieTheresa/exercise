<!DOCTYPE html>
<html>
<head>
	<title>
		SELECT(WHERE, HAVING, ORDER BY, GROUP BY, INNER JOIN, OUTER JOIN)
	</title>

	<style type="text/css">
		
		table,th,tr,td {
			padding: 3px;
		}
	</style>

</head>
<body>
	<center>
	<br><br>
			
		<form method="POST">

			Search by ID:  <input type="number" name="txtsearch">
			<button name="btnsrch">
				Search
			</button>	
			<br><br>
			
			<button type="submit" name="btninjoin">
				Inner Join
			</button>
			<br><br>

			Order Table by(des): <select name="selorderby">
				<option>ID</option>
				<option>Name</option>
				<option>Age</option>
				<option>Gender</option>
				<option>Occupation</option>
			</select>

			<button type="submit" name="btnordrby">
				Order
			</button>
			<br><br>
			Select data having age more than(grouped by occupation): <input type="number" name="txthaving">
			<button type="submit" name="btnhaving">
				Sort
			</button>
			<br><br>
			<button type="submit" name="btnlftjn">
				Left Join
			</button>
			<button type="submit" name="btnrghtjn">
				Right Join
			</button>

			<br><br><br>

			Original Table(JOIN):
		
		</form>

		<?php
		function opencon(){
				$dbhost = "localhost";
				$dbuser = "root";
				$password = "";
				$db = "sampledb";

				$conn = new mysqli($dbhost, $dbuser, $password, $db) or die("Connection Failed." . $conn -> error);
				return  $conn;
		}

				function closecon($conn){
				$conn -> close();
				}

			$con = opencon();

			$result = "SELECT * FROM table1 JOIN table2 on table1.id = table2.table1id";
				$sql = $con -> query($result);
				echo"<center><table border = '1'>";
				echo "<tr><th>ID</th> <th>Name</th><th>Age</th><th>Gender</th><th>Table2id</th><th>Table1id</th><th>Occupation</th></tr>";

				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['id'] . "</td>";
				echo "<td>" . $row['name'] . "</td>";
				echo "<td>" . $row['age'] . "</td>";
				echo "<td>" . $row['gender'] . "</td>";
				echo "<td>" . $row['table2id'] . "</td>";
				echo "<td>" . $row['table1id'] . "</td>";
				echo "<td>" . $row['occupation'] . "</td>";
				echo "</tr>";	
				}
				echo "</table>";
			if(isset($_POST['txtsearch'])){
				$id= $_POST['txtsearch'];
			}
			if(!isset($_POST['btnsrch'])){

			}else
			{
			if (empty($_POST['txtsearch'])){
					echo "<br> <br> Please enter a valid ID.";
			}else {
							$result = "SELECT * FROM table1 INNER JOIN table2 on table1.id = table2.table1id where table1.id = $id";
				$sql = $con -> query($result);
				echo"<center><br><br><table border = '1'>
				<tr><th>ID</th><th>Name</th><th>Age</th><th>Gender</th></tr>";
			
				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['id'] . "</td>";
				echo "<td>" . $row['name'] . "</td>";
				echo "<td>" . $row['age'] . "</td>";
				echo "<td>" . $row['gender'] . "</td>";
				echo "</tr>";	
				
			}
			echo "</table>";

					}
			
			}
			
			if(!isset($_POST['btninjoin'])){
				// echo "No Data to Show.";
			}else
			{	

				$result = "SELECT * FROM table1 INNER JOIN table2 on table1.id = table2.table1id";
				$sql = $con -> query($result);
				echo"<center><br><br><table border = '1'> 
				<tr><th>ID</th><th>Name</th><th>Age</th><th>Gender</th></tr>";
			
				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['id'] . "</td>";
				echo "<td>" . $row['name'] . "</td>";
				echo "<td>" . $row['age'] . "</td>";
				echo "<td>" . $row['gender'] . "</td>";
				echo "</tr>";	
				
			}
			echo "</table>";

			}
					$selordrby = "";
				if(isset($_POST['selorderby'])){
					$selordrby = $_POST['selorderby'];

					if(isset($_POST['btnordrby'])){
							$result = "SELECT * FROM table1 JOIN table2 on table1.id = table2.table1id ORDER BY $selordrby DESC";

				$sql = $con -> query($result);
				echo"<center><br><br><table border = '1'>";
				echo "<tr onclick='myfunction(this)'><th>ID</th> <th>Name</th><th>Age</th><th>Gender</th><th>Table2id</th><th>Table1id</th><th>Occupation</th></tr>";

				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['id'] . "</td>";
				echo "<td>" . $row['name'] . "</td>";
				echo "<td>" . $row['age'] . "</td>";
				echo "<td>" . $row['gender'] . "</td>";
				echo "<td>" . $row['table2id'] . "</td>";
				echo "<td>" . $row['table1id'] . "</td>";
				echo "<td>" . $row['occupation'] . "</td>";
				echo "</tr>";	
				}
				echo "</table>";
					}
				}
				
				$cnthaving = "";
				if(isset($_POST['btnhaving'])){
					
					if(empty($_POST['txthaving'])){
							echo "<br><br> Please enter a valid age.";
					}else{
					$cnthaving = $_POST['txthaving'];

					if(isset($_POST['btnhaving'])){
							$result = "SELECT COUNT(id), age, occupation FROM table1 JOIN table2 on table1.id = table2.table1id GROUP BY occupation HAVING age > $cnthaving";

				$sql = $con -> query($result);
				echo"<center><br><br><table border = '1'>";
				echo "<tr onclick='myfunction(this)'><th>Count</th><th>Occupation</th></tr>";

				while ($row = $sql->fetch_assoc()){

				echo "<tr>";
				echo "<td>" . $row['COUNT(id)'] . "</td>";
				echo "<td>" . $row['occupation'] . "</td>";
				echo "</tr>";	
				}
				echo "</table>";
					}}

				}

				if(isset($_POST['btnlftjn'])){
					$result = "SELECT * FROM table1 LEFT OUTER JOIN table2 on table1.id = table2.table1id";
					$sql = $con -> query($result);
					echo"<center><br><br><table border = '1'>
					<tr><th>ID</th><th>Name</th><th>Age</th><th>Gender</th></tr>";
				
					while ($row = $sql->fetch_assoc()){

					echo "<tr>";
					echo "<td>" . $row['id'] . "</td>";
					echo "<td>" . $row['name'] . "</td>";
					echo "<td>" . $row['age'] . "</td>";
					echo "<td>" . $row['gender'] . "</td>";
					echo "</tr>";	
					
				}
				echo "</table>";

				}

				if(isset($_POST['btnrghtjn'])){
					
						$result = "SELECT * FROM table1 RIGHT OUTER JOIN table2 on table1.id = table2.table1id";
					$sql = $con -> query($result);
					echo"<center><br><br><table border = '1'>
					<tr><th>Table ID 2</th><th>Table ID 2</th><th>Occupation</th></tr>";
				
					while ($row = $sql->fetch_assoc()){

					echo "<tr>";
					echo "<td>" . $row['table2id'] . "</td>";
					echo "<td>" . $row['table1id'] . "</td>";
					echo "<td>" . $row['occupation'] . "</td>";
					echo "</tr>";	
					
						}
						echo "</table>";
					}

					

				

			closecon($con);
			?>


</body>
</html>